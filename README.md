[![License BSD](https://img.shields.io/badge/License-BSD-lightgrey.svg)](/LICENSE)

# androidstudio-docker #

Run Android Studio from a docker container.

### Quick Start ###

```sh
git clone https://gitlab.com/commando.brando2k/androidstudio-docker.git
cd androidstudio-docker
./docker-run build
./docker-run
```

### License ###

Copyright © 2017 B.A. Parker \<commando.brando2k@gmail.com\>

The work in this repository is licensed under the BSD 2-Clause license unless 
otherwise stated.

See [License](/License) for details.
